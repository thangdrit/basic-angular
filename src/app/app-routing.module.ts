import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { AdminGuard, AuthGuard } from './auth/auth.guard'
import { LogoutComponent } from './auth/login/logout.component';
import { AuthComponent } from './auth/auth.component';
import { CategoriesComponent } from './admin/categories/categories.component';
import { ProductsComponent } from './admin/products/products.component';
import { RolesComponent } from './admin/roles/roles.component';
import { UsersComponent } from './admin/users/users.component';
import { OrdersComponent } from './admin/orders/orders.component';
const routes: Routes = [
  { path: 'auth', component: AuthComponent, canActivate: [AuthGuard] },
  { path: 'admin', component: AdminComponent, canActivate: [AdminGuard] },
  { path: 'logout', component: LogoutComponent },
  { path: 'admin/categories', component: CategoriesComponent, canActivate: [AdminGuard] },
  { path: 'admin/products', component: ProductsComponent, canActivate: [AdminGuard] },
  { path: 'admin/roles', component: RolesComponent, canActivate: [AdminGuard] },
  { path: 'admin/users', component: UsersComponent, canActivate: [AdminGuard] },
  { path: 'admin/orders', component: OrdersComponent, canActivate: [AdminGuard] },
  { path: '', redirectTo: '/auth', pathMatch: 'full' },
  { path: '**', redirectTo: '/auth' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
