import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { map, switchMap } from 'rxjs';
import { CategoriesService } from './categories.service';
import { DialogComponent } from './dialog/dialog.component';
import { Category } from './categories.component.i'
import * as moment from 'moment';
@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements AfterViewInit {
  category: Category[] = [];
  value = "";
  displayedColumns: string[] = ['id', 'name', 'created_at', 'updated_at', 'edit', 'delete'];
  dataSource = new MatTableDataSource<Category>;

  constructor(private snackBar: MatSnackBar, public dialog: MatDialog, private categoriesService: CategoriesService) { }

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  name = "";
  totalRecords = 0;
  searchText = '';
  isLoadingResults = true;

  ngAfterViewInit() {
    this.initialLoad('')
    this.pageChange()
  }

  CreateDialog(): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '500px',
      data: { category_name: this.name, title: "Create New Category", buttonTitle: "Create" },
    },);

    dialogRef.afterClosed().subscribe(result => {
      this.name = result;
      if (this.name !== "" && this.name !== null && this.name !== undefined) {
        this.categoriesService.Create(this.name).subscribe((res) => {
          this.name = "";
          this.initialLoad('');
        });
      } else if (this.name === "") {
        this.snackBar.open("Không được để trống tên Category", "Đóng", { duration: 1000 });
      } else if (this.name === null) {
        this.name = "";
      }
    });
  }

  EditDialog(id: number, name: string): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '500px',
      data: { category_name: name, title: "Edit Category", buttonTitle: "Save" },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.name = result;
      if (this.name !== "" && this.name !== null && this.name !== undefined) {
        this.categoriesService.Edit(id, this.name).subscribe((res) => {
          this.name = "";
          this.initialLoad('');
        });
      } else if (this.name === "") {
        this.snackBar.open("Không được để trống tên Category", "Đóng", { duration: 1000 });
      } else if (this.name === null) {
        this.name = "";
      }
    });
  }

  Delete(id: number) {
    this.categoriesService.Delete(id).subscribe((res) => {
      if (res === undefined) {
        this.initialLoad('');
        this.searchText = '';
      }
    })
  }

  initialLoad(search: string) {
    this.isLoadingResults = true;
    let currentPage: number;
    currentPage = (this.paginator?.pageIndex ?? 0) + 1;
    this.categoriesService.GetWithPage(currentPage, (this.paginator?.pageSize ?? 0), search)
      .subscribe((result) => {
        this.isLoadingResults = false;
        const data = result as {
          totalPage: number,
          totalCount: number; data: Category[]
        };
        this.totalRecords = data.totalCount;
        this.category = data.data;
        var source = this.category.map(c => ({
          id: c.id,
          name: c.name,
          created_at: moment(c.created_at).format('DD/MM/yyyy - HH:mm:ss'),
          updated_at: moment(c.created_at).format('DD/MM/yyyy - HH:mm:ss'),
        }));
        if (currentPage > data.totalPage && search === '') {
          this.paginator.pageIndex = this.paginator.pageIndex - 1;
          this.initialLoad('');
        } else {
          this.dataSource = new MatTableDataSource(source);
        }
      })
  }

  pageChange() {
    this.paginator?.page.pipe(
      switchMap(() => {
        this.isLoadingResults = true;
        let currentPage = (this.paginator?.pageIndex ?? 0) + 1;
        return this.categoriesService.GetWithPage(currentPage, (this.paginator?.pageSize ?? 0), this.searchText);
      }),
      map(result => {
        const data = result as { totalCount: number, data: Category[] };
        this.totalRecords = data.totalCount;
        return data.data;
      })
    ).subscribe(data => {
      this.category = data;
      this.isLoadingResults = false;
      var source = this.category.map(c => ({
        id: c.id,
        name: c.name,
        created_at: moment(c.created_at).format('DD/MM/yyyy - HH:mm:ss'),
        updated_at: moment(c.created_at).format('DD/MM/yyyy - HH:mm:ss'),
      }));
      this.dataSource = new MatTableDataSource(source);
    });

  }

  Search(key: string) {
    this.initialLoad(key)
    this.searchText = key;
    this.paginator.firstPage();
  }
}
