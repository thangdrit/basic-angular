export interface CategoryDialogData {
    category_name: string;
    title: string;
    buttonTitle: string;
    err: boolean;
}