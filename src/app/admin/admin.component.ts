import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})

export class AdminComponent implements OnInit {
  items = [
    {
      name: 'Categories',
      link: '/admin/categories',
      icon: 'power_input',
      selected: false
    },
    {
      name: 'Products',
      link: '/admin/products',
      icon: 'store',
      selected: false
    },
    {
      name: 'Roles',
      link: '/admin/roles',
      icon: 'verified_user',
      selected: false
    },
    {
      name: 'Users',
      link: '/admin/users',
      icon: 'portrait',
      selected: false
    },
    {
      name: 'Orders',
      link: '/admin/orders',
      icon: 'add_shopping_cart',
      selected: false
    },
  ];
  selected: string | undefined;
  constructor(private router: Router) {
  }

  ngOnInit(): void {
    let name = localStorage.getItem('page');
    for (let i = 0; i < this.items.length; i++) {
      if (this.items[i].name === name) {
        this.items[i].selected = true;
        let link = this.items[i].link;
        this.router.navigate(['/' + link]);
      } else {
        this.items[i].selected = false;
      }

    }
  }

  set(item: any) {
    localStorage.setItem('page', item.name);
  }
}
