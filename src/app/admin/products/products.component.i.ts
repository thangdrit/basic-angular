export interface Product {
    id: number;
    name: string;
    description: string;
    price: string;
    image: string;
    category: [];
    created_at: string;
    updated_at: string;
}