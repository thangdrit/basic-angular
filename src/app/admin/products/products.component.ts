import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { map, switchMap } from 'rxjs';
import { DialogComponent } from '../categories/dialog/dialog.component';
import { DialogProductComponent } from './dialog/dialog.component';
import { Product } from './products.component.i';
import { ProductsService } from './products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements AfterViewInit {
  product: Product[] = [];
  displayedColumns: string[] = ['id', 'name', 'price', 'image', 'category', 'created_at', 'updated_at', 'edit', 'delete'];
  dataSource = new MatTableDataSource<Product>;
  isLoadingResults = true;

  constructor(private router: Router, private snackBar: MatSnackBar, public dialog: MatDialog, private productsService: ProductsService) { }

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  totalRecords = 0;
  searchText = '';

  ngAfterViewInit() {
    this.initialLoad('', '')
    this.pageChange()
  }

  initialLoad(search: string, role: string) {
    this.isLoadingResults = true;
    let currentPage: number;
    currentPage = (this.paginator?.pageIndex ?? 0) + 1;
    if (search !== '') currentPage = 1;
    this.productsService.GetWithPage(currentPage, (this.paginator?.pageSize ?? 0), search, role)
      .subscribe((result) => {
        this.isLoadingResults = false;
        const data = result as {
          totalPage: number,
          totalCount: number; data: Product[]
        };
        this.totalRecords = data.totalCount;
        this.product = data.data;

        var source = this.product.map(c => ({
          id: c.id,
          name: c.name,
          description: c.description,
          price: c.price,
          image: c.image,
          category: c.category,
          created_at: moment(c.created_at).format('DD/MM/yyyy - HH:mm:ss'),
          updated_at: moment(c.created_at).format('DD/MM/yyyy - HH:mm:ss'),
        }));

        if (currentPage > data.totalPage && search === '') {
          this.paginator.pageIndex = this.paginator.pageIndex - 1;
          this.initialLoad('', '');
        } else {
          this.dataSource = new MatTableDataSource(source);
        }
      })
  }

  pageChange() {
    this.paginator?.page.pipe(
      switchMap(() => {
        this.isLoadingResults = true;
        let currentPage = (this.paginator?.pageIndex ?? 0) + 1;
        return this.productsService.GetWithPage(currentPage, (this.paginator?.pageSize ?? 0), this.searchText, '');
      }),
      map(result => {
        const data = result as { totalCount: number, data: Product[] };
        this.totalRecords = data.totalCount;
        return data.data;
      })
    ).subscribe(data => {
      this.product = data;
      this.isLoadingResults = false;
      var source = this.product.map(c => ({
        id: c.id,
        name: c.name,
        description: c.description,
        price: c.price,
        image: c.image,
        category: c.category,
        created_at: moment(c.created_at).format('DD/MM/yyyy - HH:mm:ss'),
        updated_at: moment(c.created_at).format('DD/MM/yyyy - HH:mm:ss'),
      }));
      this.dataSource = new MatTableDataSource(source);
    });
  }

  Create(): void {
    const dialogRef = this.dialog.open(DialogProductComponent, {
      width: '800px',
      data: { title: "Create New Product", buttonTitle: "Create" },
    },);

    dialogRef.afterClosed().subscribe(result => {
      if (result !== null && result !== undefined) {
        let formData = new FormData();
        formData.append('name', result.name || '');
        formData.append('photo', result.image || '');
        formData.append('description', result.description || '');
        formData.append('price', result.price || '');
        formData.append('category', result.category || '');
        this.productsService.Create(formData).subscribe(() => {
          this.initialLoad('', '');
        });
      }
    });
  }

  Delete(id: number) {
    this.productsService.Delete(id).subscribe((res) => {
      if (res === undefined) {
        this.initialLoad('', '');
        this.searchText = '';
      }
    })
  }

  Edit(e: any): void {
    const dialogRef = this.dialog.open(DialogProductComponent, {
      width: '600px',
      data: { product: e, title: "Edit Product", buttonTitle: "Save" },
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        let formData = new FormData();
        formData.append('name', result.name as string);
        if (result.image) formData.append('photo', result.image as string);
        formData.append('description', result.description as string);
        formData.append('price', result.price as string);
        formData.append('category', result.category as string);
        this.productsService.Edit(e.id, formData).subscribe((res) => {
          this.initialLoad('', '');
        });
      }
    });
  }

  Search(key: string) {
    this.initialLoad(key, '')
    this.searchText = key;
    this.paginator.firstPage();
  }

  chipSelect(id: number): void {
    localStorage.setItem('page', 'Categories');
    this.router.navigate(['admin/categories']);
  }
}


