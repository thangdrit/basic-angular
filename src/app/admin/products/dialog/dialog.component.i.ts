import { Product } from "../products.component.i";

export interface DialogData {
    title: string;
    buttonTitle: string;
    product: Product;
};


export interface DialogCategory {
    id: number;
    name: string;
}


export interface cProduct {
    id: number;
    name: string;
    description: 'string';
    price: number;
    image: string;
    category: number[];
    created_at: string;
    updated_at: string;
}