import { ScrollDispatcher } from '@angular/cdk/scrolling';
import { Component, ElementRef, Inject, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { map, Observable, startWith, switchMap } from 'rxjs';
import { CategoriesService } from '../../categories/categories.service';
import { Product } from '../products.component.i';
import { cProduct, DialogCategory, DialogData } from './dialog.component.i';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogProductComponent implements OnInit {
  category: DialogCategory[] = [];
  imageProduct: string = '';
  categoryControl = new FormControl('');
  filteredCates: Observable<DialogCategory[]> | undefined;
  cates: DialogCategory[] = [];
  allCates: DialogCategory[] = [];
  @ViewChild('cateInput') cateInput!: ElementRef<HTMLInputElement>;

  constructor(
    public dialogRef: MatDialogRef<DialogProductComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    @Inject(MAT_DIALOG_DATA) public product: Product,
    @Inject(MAT_DIALOG_DATA) public cproduct: cProduct,
    private categoriesServices: CategoriesService,
    public scroll: ScrollDispatcher, private ngZone: NgZone
  ) {

  }


  add(event: MatChipInputEvent): void {
    event.chipInput!.clear();
    this.categoryControl.setValue(null);
  }

  remove(cate: DialogCategory): void {
    const index = this.cates.indexOf(cate);
    if (index >= 0) {
      this.cates.splice(index, 1);
    }

    this.allCates = [...this.allCates, cate]
    // this.allCates.filter(cate => cate)
    this.filteredCates = this.categoryControl.valueChanges.pipe(
      startWith(null),
      map((cate: string | null) => (cate ? this._filter(cate) : this.allCates.slice())),
    );
  }


  selected(event: MatAutocompleteSelectedEvent): void {
    this.exclude(event.option.value)
    this.cates.push(event.option.value);
    this.cateInput.nativeElement.value = '';
    this.categoryControl.setValue(null);

  }

  private exclude(cate: DialogCategory): void {
    const b = this.allCates.filter(c => {
      return c.id !== cate.id
    })
    this.allCates = b;
  }

  private _filter(value: string): DialogCategory[] {
    const filterValue = value.toString().toLowerCase();
    return this.allCates.filter(cate => cate.name.toLowerCase().includes(filterValue));
  }


  myForm = new FormGroup({
    fileSource: new FormControl('File', [Validators.required]),
    fileControl: new FormControl(),
    name: new FormControl(),
    price: new FormControl(),
    description: new FormControl(),
  });

  ngOnInit(): void {
    let a = true;
    if (this.data.buttonTitle === "Save") {
      this.myForm.patchValue(this.data.product)
      if (this.data.product.category) this.cates = [...this.data.product.category]
      this.imageProduct = this.data.product.image;
    }
    this.getList();
  }

  getList() {
    this.categoriesServices.GetWithPage(1, 100, '')
      .subscribe((result) => {
        const data = result as {
          totalPage: number,
          totalCount: number; data: DialogCategory[]
        };
        this.category = data.data;
        const list = this.category.map(c => ({
          id: c.id,
          name: c.name,
        }))
        this.allCates = list;
        this.cates.forEach(c => {
          this.exclude(c);
        })
        this.filteredCates = this.categoryControl.valueChanges.pipe(
          startWith(null),
          map((cate: string | null) => (cate ? this._filter(cate) : this.allCates.slice())),
        );
      });
  }
  onNoClick(): void {
    this.dialogRef.close();

  }

  onFileSelected() {
    this.cproduct.image = this.myForm.get('fileControl')?.value;
    const reader = new FileReader();
    reader.onload = (e: any) => {
      this.imageProduct = e.target.result;
    };
    reader.readAsDataURL(this.myForm.get('fileControl')?.value);
  }

  submit(): void {
    this.cproduct.name = this.myForm.get('name')?.value;
    this.cproduct.price = this.myForm.get('price')?.value;
    this.cproduct.description = this.myForm.get('description')?.value;
    this.cproduct.category = this.cates.map(c => c.id);

  }


}


