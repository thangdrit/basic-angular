import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { catchError, map, Observable, of, switchMap } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ProductsService {
    constructor(private snackBar: MatSnackBar, private http: HttpClient) { }
    handleError(error: HttpErrorResponse) {
        this.snackBar.open("Result", error.error.message, { duration: 2000 });
        return error.error.message;
    }


    GetWithPage(page: number, size: number, search: string, categories: string) {
        const Url = `http://localhost:8000/api/products?page=${page}&limit=${size}&search=${search}&category=[${categories}]`;
        return this.http.get(Url).pipe(
            map((rs) => {
                return rs;
            }),
            catchError(this.handleError.bind(this)));

    }

    GetOne(id: number) {
        const Url = `http://localhost:8000/api/products/${id}`;
        return this.http.get(Url).pipe(
            map((rs) => {
                return rs;
            }),
            catchError(this.handleError.bind(this)));
    }

    Create(data: FormData) {
        const Url = 'http://localhost:8000/api/products';
        return this.http.post(Url, data).pipe(
            map((rs) => {
                const m = rs as { message: string };
                this.snackBar.open("Notification", m.message, { duration: 2000 });
                return rs;
            }),
            catchError(this.handleError.bind(this))
        );
    }
    Edit(id: number, data: FormData) {
        const Url = `http://localhost:8000/api/products/${id}`;
        return this.http.put(Url, data).pipe(
            map((rs) => {
                const m = rs as { message: string };
                this.snackBar.open("Notification", m.message, { duration: 2000 });
                return rs;
            }),
            catchError(this.handleError.bind(this))
        );
    }

    Delete(id: number) {
        const Url = `http://localhost:8000/api/products/${id}`;
        return this.http.delete(Url).pipe(
            map((rs) => {
                const ms = rs as { message: string }
                this.snackBar.open("Result", ms.message, { duration: 2000 });
            }),
            catchError(this.handleError.bind(this))
        )

    }
}


