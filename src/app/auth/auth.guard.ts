import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
import { LoginService } from './login/login.service';
@Injectable({ providedIn: 'root' })
export class AdminGuard implements CanActivate {

    constructor(private authService: AuthService, private router: Router) { }
    //for Admin page
    canActivate() {
        if (localStorage.getItem('token')) {
            this.authService.Auth().subscribe();
            return true;
        } else {
            this.router.navigate(['/auth']);
            return false;
        }
    }
}
@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }
    //for Auth Pages
    canActivate() {
        if (localStorage.getItem('token')) {
            this.router.navigate(['/admin']);
            return false;
        } else {
            return true;
        }
    }
}