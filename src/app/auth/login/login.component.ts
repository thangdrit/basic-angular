import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { LoginService } from './login.service';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hide = true;
  ngOnInit(): void {
  }
  constructor(private loginService: LoginService, private formBuilder: FormBuilder) { }
  emailFormControl = new FormControl('', [Validators.required, Validators.email]);
  passwordFormControl = new FormControl('', [Validators.required, Validators.minLength(6)]);
  matcher = new ErrorStateMatcher();

  Login() {
    const email = this.emailFormControl.value;
    const password = this.passwordFormControl.value;
    return this.loginService.Login(email, password).subscribe();
  }

}
