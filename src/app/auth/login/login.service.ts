import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private router: Router, private snackBar: MatSnackBar, private http: HttpClient) {
  }

  handleError(error: HttpErrorResponse) {
    this.snackBar.open("Result", error.error.message, { duration: 2000 });
    return error.error.message;
  }

  Login(email: any, password: any) {
    const Url = 'http://localhost:8000/api/auth/login';
    return this.http.post(Url, { email, password }).pipe(
      map((response) => {
        if (response) {
          const m = response as { message: string };
          const token = response as { access_token: string }
          localStorage.setItem('token', token.access_token);
          this.snackBar.open("Notification", m.message, { duration: 1000 });
          this.router.navigate(['admin']);
        }

      }),
      catchError(this.handleError.bind(this))

    )
  }
}
