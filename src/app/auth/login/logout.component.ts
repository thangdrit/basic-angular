import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
    selector: 'app-logout',
    template: '',
})
export class LogoutComponent implements OnInit {
    constructor(private snackBar: MatSnackBar, private router: Router) { }
    ngOnInit(): void {
        if (localStorage.getItem('token')) {
            localStorage.removeItem('token');
            localStorage.removeItem('page');
            this.snackBar.open("Notification", "Logout Successful", { duration: 1000 });

        }
        this.router.navigate(['/auth']);
    }
}
