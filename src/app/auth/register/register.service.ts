import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { catchError, map } from 'rxjs';
import { LoginService } from '../login/login.service';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private loginService: LoginService, private snackBar: MatSnackBar, private http: HttpClient) { }
  handleError(error: HttpErrorResponse) {
    this.snackBar.open("Result", error.error.message, { duration: 2000 });
    return error.error.message;
  }
  Register(email: any, password: any, firstname: any, lastname: any) {
    const Url = 'http://localhost:8000/api/auth/register';
    return this.http.post(Url, { email, password, firstname, lastname }).pipe(
      map((response) => {
        if (response) {
          const m = response as { message: string };
          this.snackBar.open("Notification", m.message, { duration: 1000 });
          this.loginService.Login(email, password).subscribe();
        }

      }),
      catchError(this.handleError.bind(this))

    )
  }
}
