import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { RegisterService } from './register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  match = false;
  constructor(private registerService: RegisterService) { }
  emailFormControl = new FormControl('', [Validators.required, Validators.email]);
  passwordFormControl = new FormControl('', [Validators.required, Validators.minLength(6)]);
  cpasswordFormControl = new FormControl('', [Validators.required]);
  firstnameFormControl = new FormControl('', [Validators.required]);
  lastnameFormControl = new FormControl('');
  matcher = new ErrorStateMatcher();

  ngOnInit(): void {
  }

  Check() {

    if (this.passwordFormControl.value === this.cpasswordFormControl.value) {
      return null;

    } else {
      return this.cpasswordFormControl.setErrors(['passwordNotMatch']);
    }
    console.log()
  }
  Register() {

    const email = this.emailFormControl.value;
    const password = this.passwordFormControl.value;
    const firstname = this.firstnameFormControl.value;
    const lastlname = this.lastnameFormControl.value;
    return this.registerService.Register(email, password, firstname, lastlname).subscribe();
  }
}
